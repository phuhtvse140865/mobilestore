/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.phuhtv.StoreMobileDemo;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author tsuna
 */
public interface ProductRepository extends JpaRepository<Product, Long>{
    
    
}
