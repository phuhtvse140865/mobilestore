<%-- 
    Document   : index
    Created on : May 31, 2022, 9:03:24 AM
    Author     : tsuna
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link
      href="https://kit-pro.fontawesome.com/releases/v5.15.4/css/pro.min.css"
      rel="stylesheet"
    />
    <link rel="stylesheet" href="/css/ListProduct.css" />
    <title>List Product</title>
  </head>
  <style>
      body {
  margin: 0;
  padding: 0;
}

.all_page_listProduct {
  width: 100%;
  height: 100%;
}

.head_page_listProduct {
  height: 170px;
  background-color: rgb(222, 222, 222);
  margin: 0 0 30px 0;
}

.out_viewCart {
  text-align: right;
  padding: 40px 40px 10px 0;
}

.button_viewCart {
  height: 35px;
  border-radius: 5px;
  border: none;
  font-size: 16px;
  cursor: pointer;
  border: 1px solid black;
}

.head_title {
  font-size: 50px;
  margin: -2% 5% 0;
}

.head_title_p {
  margin: 0 5%;
  font-size: 18px;
}

.body_page_listProduct {
  width: 90%;
  height: auto;
  margin: 0 5%;
  display: flex;
}

.product_view {
  width: 22.3%;
  margin-left: 2%;
  border: 1px solid rgb(188, 188, 188);
  border-radius: 5px;
  font-family: "Lucida Sans", "Lucida Sans Regular", "Lucida Grande",
    "Lucida Sans Unicode", Geneva, Verdana, sans-serif;
}

.view_head {
  width: 90%;
  height: auto;
  margin: 3% 5% 0;
  font-size: 22px;
}

.view_body {
  width: 90%;
  height: auto;
  margin: 1% 5% 0;
}
p {
  margin: 5px;
  font-size: 15px;
}
.Img_view img {
  width: 60%;
  height: 250px;
  margin: 0 20%;
}
.out_price_view {
  display: flex;
  height: 20px;
}

.out_quantity_view {
  display: flex;
}
.view_foot {
  width: 90%;
  height: auto;
  margin: 4% 5% 5%;
}
.status {
  display: flex;
}
.button_details {
  width: 100px;
  height: 30px;
  margin-left: 10px;
  margin-right: 30px;
  background-color: rgb(59, 137, 254);
  border: none;
  border-radius: 5px;
  color: white;
  font-size: 15px;
}

.button_order {
  width: 120px;
  height: 30px;
  background-color: orange;
  border: none;
  border-radius: 5px;
  color: white;
  font-size: 15px;
}

  </style>
  <body>
    <div class="all_page_listProduct">
      <div class="head_page_listProduct">
        <div class="out_viewCart">
          <button class="button_viewCart">
            <i class="fas fa-shopping-cart"></i> &nbspView Cart
          </button>
        </div>
        <div class="head_title">Product</div>
        <p class="head_title_p">All the available product in our store</p>
      </div>
      <div class="body_page_listProduct">
        <div class="product_view">
          <div class="view_head">Iphone 12 Pro Max</div>
          <div class="view_body">
            <div class="Img_view">
              <img
                src="https://bizweb.dktcdn.net/100/398/430/products/iphone-12-pro-family-hero.jpg?v=1602999427703"
                alt="Iphone12ProMax"
              />
            </div>
            <p class="disciption_view">
              The 256 GB iPhone 12 Pro Max is a smartphone with many outstanding
              features with a quality camera system, outstanding performance or
              support for 5G connectivity, which promises to be a product that
              brings the ultimate experience. user advantage.
            </p>
            <div class="out_price_view">
              <p class="price_view">1099&nbsp</p>
              <p class="price_unit">USD</p>
            </div>
            <div class="out_quantity_view">
              <p class="quantity_view">800&nbsp</p>
              <p class="quantity_unit">units in stock</p>
            </div>
          </div>
          <div class="view_foot">
            <div class="status">
              <div class="out_details">
                <button class="button_details">
                  <i class="fas fa-info-circle"></i>&nbspDetails
                </button>
              </div>
              <div class="out_order">
                <button class="button_order">
                  <i class="fas fa-shopping-cart"></i>&nbspOrder Now
                </button>
              </div>
            </div>
          </div>
        </div>
        <div class="product_view">
          <div class="view_head">Iphone 12 Pro Max</div>
          <div class="view_body">
            <div class="Img_view">
              <img
                src="https://bizweb.dktcdn.net/100/398/430/products/iphone-12-pro-family-hero.jpg?v=1602999427703"
                alt="Iphone12ProMax"
              />
            </div>
            <p class="disciption_view">
              The 256 GB iPhone 12 Pro Max is a smartphone with many outstanding
              features with a quality camera system, outstanding performance or
              support for 5G connectivity, which promises to be a product that
              brings the ultimate experience. user advantage.
            </p>
            <div class="out_price_view">
              <p class="price_view">1099&nbsp</p>
              <p class="price_unit">USD</p>
            </div>
            <div class="out_quantity_view">
              <p class="quantity_view">800&nbsp</p>
              <p class="quantity_unit">units in stock</p>
            </div>
          </div>
          <div class="view_foot">
            <div class="status">
              <div class="out_details">
                <button class="button_details">
                  <i class="fas fa-info-circle"></i>&nbspDetails
                </button>
              </div>
              <div class="out_order">
                <button class="button_order">
                  <i class="fas fa-shopping-cart"></i>&nbspOrder Now
                </button>
              </div>
            </div>
          </div>
        </div>
        <div class="product_view">
          <div class="view_head">Iphone 12 Pro Max</div>
          <div class="view_body">
            <div class="Img_view">
              <img
                src="https://bizweb.dktcdn.net/100/398/430/products/iphone-12-pro-family-hero.jpg?v=1602999427703"
                alt="Iphone12ProMax"
              />
            </div>
            <p class="disciption_view">
              The 256 GB iPhone 12 Pro Max is a smartphone with many outstanding
              features with a quality camera system, outstanding performance or
              support for 5G connectivity, which promises to be a product that
              brings the ultimate experience. user advantage.
            </p>
            <div class="out_price_view">
              <p class="price_view">1099&nbsp</p>
              <p class="price_unit">USD</p>
            </div>
            <div class="out_quantity_view">
              <p class="quantity_view">800&nbsp</p>
              <p class="quantity_unit">units in stock</p>
            </div>
          </div>
          <div class="view_foot">
            <div class="status">
              <div class="out_details">
                <button class="button_details">
                  <i class="fas fa-info-circle"></i>&nbspDetails
                </button>
              </div>
              <div class="out_order">
                <button class="button_order">
                  <i class="fas fa-shopping-cart"></i>&nbspOrder Now
                </button>
              </div>
            </div>
          </div>
        </div>
        <div class="product_view">
          <div class="view_head">Iphone 12 Pro Max</div>
          <div class="view_body">
            <div class="Img_view">
              <img
                src="https://bizweb.dktcdn.net/100/398/430/products/iphone-12-pro-family-hero.jpg?v=1602999427703"
                alt="Iphone12ProMax"
              />
            </div>
            <p class="disciption_view">
              The 256 GB iPhone 12 Pro Max is a smartphone with many outstanding
              features with a quality camera system, outstanding performance or
              support for 5G connectivity, which promises to be a product that
              brings the ultimate experience. user advantage.
            </p>
            <div class="out_price_view">
              <p class="price_view">1099&nbsp</p>
              <p class="price_unit">USD</p>
            </div>
            <div class="out_quantity_view">
              <p class="quantity_view">800&nbsp</p>
              <p class="quantity_unit">units in stock</p>
            </div>
          </div>
          <div class="view_foot">
            <div class="status">
              <div class="out_details">
                <button class="button_details">
                  <i class="fas fa-info-circle"></i>&nbspDetails
                </button>
              </div>
              <div class="out_order">
                <button class="button_order">
                  <i class="fas fa-shopping-cart"></i>&nbspOrder Now
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
